DOCKER_COMPOSE := docker-compose -f docker-compose.yml
CS_FIXER := vendor/friendsofphp/php-cs-fixer/php-cs-fixer
CODE_SNIFFER := vendor/squizlabs/php_codesniffer/bin/phpcs
CODE_SNIFFER_FIXER := vendor/squizlabs/php_codesniffer/bin/phpcbf
PHP_UNIT := ./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox

uid:
    UID := $(shell id -u)
    export UID

#Docker-compose
start: run composer-install
run:
	$(DOCKER_COMPOSE) up --build -d
down:
	$(DOCKER_COMPOSE) down
kill:
	$(DOCKER_COMPOSE) kill
recreate:
	$(DOCKER_COMPOSE) create --force-recreate --build
restart: down run

#LOGS
php-log:
	docker logs -f --details php

#PHP - Composer
composer-install:
	$(DOCKER_COMPOSE) exec -u $(UID):$(UID) php composer install
composer-update:
	$(DOCKER_COMPOSE) exec -u $(UID):$(UID) php composer update
composer-require:
ifdef COMPOSERLIBRARY
	$(DOCKER_COMPOSE) exec -u $(UID):$(UID) php composer require $(COMPOSERLIBRARY)
endif

#CodeSniffer
code-sniffer:
	$(DOCKER_COMPOSE) exec -T -u $(UID):$(UID) php $(CODE_SNIFFER) --standard=PSR12 -s -p ./src
code-sniffer-tests:
	$(DOCKER_COMPOSE) exec -T -u $(UID):$(UID) php $(CODE_SNIFFER) --standard=PSR12 -s -p ./tests
code-sniffer-fix:
	$(DOCKER_COMPOSE) exec -u $(UID):$(UID) php $(CODE_SNIFFER_FIXER) --standard=PSR12 -s -p ./src
code-sniffer-fix-tests:
	$(DOCKER_COMPOSE) exec -u $(UID):$(UID) php $(CODE_SNIFFER_FIXER) --standard=PSR12 -s -p ./tests

#PHPUnit:
php-unit-all:
	$(DOCKER_COMPOSE) exec php $(PHP_UNIT) --do-not-cache-result

bash:
	$(DOCKER_COMPOSE) exec php /bin/sh; exit 0;