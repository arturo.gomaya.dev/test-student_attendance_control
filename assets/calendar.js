import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import esLocale from '@fullcalendar/core/locales/es';

global.LoadCalendar = (events) => {
    var calendarEl = document.getElementById('calendar');
    let calendar = new Calendar(calendarEl,{
        plugins: [ dayGridPlugin, timeGridPlugin ],
        headerToolbar: {
            left: 'dayGridMonth,timeGridWeek',
            center: 'title',
            right: 'prev,today,next',
        },
        locale: esLocale,
        initialDate: '2018-03-01',
        events: events
    });
    calendar.render();
}