<?php

declare(strict_types=1);

namespace App\Tests\Application\Query\Session\GetSessionsByClassRoomGroup;

use App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto\SessionDto;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto\SessionsDto;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\GetSessionsByClassRoomGroupHandler;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\GetSessionsByClassRoomGroupQuery;
use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Session\Session;
use App\Domain\Model\Session\ValueObject\SessionEndDate;
use App\Domain\Model\Session\ValueObject\SessionId;
use App\Domain\Model\Session\ValueObject\Sessions;
use App\Domain\Model\Session\ValueObject\SessionStartDate;
use App\Domain\Service\Session\SessionsFinder;
use PHPUnit\Framework\TestCase;

class GetSessionsByClassRoomGroupHandlerTest extends TestCase
{
    public function test_given_query_to_handler_when_execute_then_return_dto()
    {
        $session = Session::from(
            SessionId::from(1),
            ClassRoomGroup::from(
                ClassRoomId::from(1),
                GroupId::from(1)
            ),
            SessionStartDate::from('now'),
            SessionEndDate::from('now'),
        );
        $sessions = Sessions::from([$session]);

        $sessionFinder = $this->createMock(SessionsFinder::class);
        $sessionFinder
            ->expects(self::once())
            ->method('execute')
            ->with(
                $session->classRoomGroup()->classRoomId(),
                $session->classRoomGroup()->groupId(),
            )
            ->willReturn($sessions);

        $handler = new GetSessionsByClassRoomGroupHandler($sessionFinder);
        $query = new GetSessionsByClassRoomGroupQuery(
            $session->classRoomGroup()->classRoomId()->value(),
            $session->classRoomGroup()->groupId()->value(),
        );
        $sessionsDto = $handler($query);

        self::assertInstanceOf(SessionsDto::class,$sessionsDto);
        self::assertCount($sessions->count(),$sessionsDto);
        self::assertEquals(
            SessionDto::fromSession($session),
            $sessionsDto->toArray()[0],
        );
    }
}
