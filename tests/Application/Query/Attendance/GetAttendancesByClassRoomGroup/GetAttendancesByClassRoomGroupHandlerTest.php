<?php

declare(strict_types=1);

namespace App\Tests\Application\Query\Attendance\GetAttendancesByClassRoomGroup;

use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\Dto\AttendanceDto;
use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\Dto\AttendancesDto;
use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\GetAttendancesByClassRoomGroupHandler;
use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\GetAttendancesByClassRoomGroupQuery;
use App\Domain\Model\Attendance\Attendance;
use App\Domain\Model\Attendance\ValueObject\AttendanceDate;
use App\Domain\Model\Attendance\ValueObject\AttendanceId;
use App\Domain\Model\Attendance\ValueObject\Attendances;
use App\Domain\Model\Attendance\ValueObject\AttendanceStatus;
use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Domain\Service\Attendance\AttendancesFinder;
use PHPUnit\Framework\TestCase;

class GetAttendancesByClassRoomGroupHandlerTest extends TestCase
{
    public function test_given_query_to_handler_when_execute_then_return_dto()
    {
        $attendance = Attendance::from(
            AttendanceId::from(1),
            AttendanceDate::from('now'),
            AttendanceStatus::from('2'),
            ClassRoomGroup::from(
                ClassRoomId::from(1),
                GroupId::from(1)
            ),
            StudentId::from(1),
        );
        $attendances = Attendances::from([$attendance]);

        $attendanceFinder = $this->createMock(AttendancesFinder::class);
        $attendanceFinder
            ->expects(self::once())
            ->method('execute')
            ->with(
                $attendance->classRoomGroup()->classRoomId(),
                $attendance->classRoomGroup()->groupId(),
            )
            ->willReturn($attendances);

        $handler = new GetAttendancesByClassRoomGroupHandler($attendanceFinder);
        $query = new GetAttendancesByClassRoomGroupQuery(
            $attendance->classRoomGroup()->classRoomId()->value(),
            $attendance->classRoomGroup()->groupId()->value(),
        );
        $attendancesDto = $handler($query);

        self::assertInstanceOf(AttendancesDto::class,$attendancesDto);
        self::assertCount($attendances->count(),$attendancesDto);
        self::assertEquals(
            AttendanceDto::fromAttendance($attendance),
            $attendancesDto->toArray()[0],
        );
    }
}
