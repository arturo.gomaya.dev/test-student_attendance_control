<?php

declare(strict_types=1);

namespace App\Tests\Application\Query\Student\GetStudentsByClassRoomGroup;

use App\Application\Query\Student\GetStudentsByClassRoomGroup\Dto\StudentDto;
use App\Application\Query\Student\GetStudentsByClassRoomGroup\Dto\StudentsDto;
use App\Application\Query\Student\GetStudentsByClassRoomGroup\GetStudentsByClassRoomGroupQuery;
use App\Application\Query\Student\GetStudentsByClassRoomGroup\GetStudentsByClassRoomGroupHandler;
use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\Student;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Domain\Model\Student\ValueObject\StudentLastName;
use App\Domain\Model\Student\ValueObject\StudentName;
use App\Domain\Model\Student\ValueObject\Students;
use App\Domain\Service\Student\StudentsFinder;
use PHPUnit\Framework\TestCase;

class GetStudentsByClassRoomGroupHandlerTest extends TestCase
{
    public function test_given_query_to_handler_when_execute_then_return_dto()
    {
        $student = Student::from(
            StudentId::from(1),
            StudentName::from('name'),
            StudentLastName::from('lastName'),
            ClassRoomGroup::from(
                ClassRoomId::from(1),
                GroupId::from(1)
            ),
        );
        $students = Students::from([$student]);

        $studentFinder = $this->createMock(StudentsFinder::class);
        $studentFinder
            ->expects(self::once())
            ->method('execute')
            ->with(
                $student->classRoomGroup()->classRoomId(),
                $student->classRoomGroup()->groupId(),
            )
            ->willReturn($students);

        $handler = new GetStudentsByClassRoomGroupHandler($studentFinder);
        $query = new GetStudentsByClassRoomGroupQuery(
            $student->classRoomGroup()->classRoomId()->value(),
            $student->classRoomGroup()->groupId()->value(),
        );
        $studentsDto = $handler($query);

        self::assertInstanceOf(StudentsDto::class,$studentsDto);
        self::assertCount($students->count(),$studentsDto);
        self::assertEquals(
            StudentDto::fromStudent($student),
            $studentsDto->toArray()[0],
        );
    }
}
