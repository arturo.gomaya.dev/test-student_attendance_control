<?php

declare(strict_types=1);

namespace App\Tests\Domain\Service\Session;

use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Session\Session;
use App\Domain\Model\Session\SessionRepository;
use App\Domain\Model\Session\ValueObject\SessionEndDate;
use App\Domain\Model\Session\ValueObject\SessionId;
use App\Domain\Model\Session\ValueObject\Sessions;
use App\Domain\Model\Session\ValueObject\SessionStartDate;
use App\Domain\Service\Session\SessionsFinder;
use PHPUnit\Framework\TestCase;

class SessionFinderTest extends TestCase
{
    public function test_given_data_to_sessions_finder_when_execute_then_return_session_collection()
    {
        $session = Session::from(
            SessionId::from(1),
            ClassRoomGroup::from(
                ClassRoomId::from(1),
                GroupId::from(1)
            ),
            SessionStartDate::from('now'),
            SessionEndDate::from('now'),
        );
        $sessions = Sessions::from([$session]);

        $repository = $this->createMock(SessionRepository::class);
        $repository
            ->expects(self::once())
            ->method('findAllByClassRoomGroup')
            ->with(
                $session->classRoomGroup()->classRoomId(),
                $session->classRoomGroup()->groupId(),
            )
            ->willReturn($sessions);

        $sessionsFinder = new SessionsFinder($repository);
        $result = $sessionsFinder->execute(
            $session->classRoomGroup()->classRoomId(),
            $session->classRoomGroup()->groupId(),
        );

        self::assertInstanceOf(Sessions::class, $result);
        self::assertEquals($sessions,$result);
    }
}
