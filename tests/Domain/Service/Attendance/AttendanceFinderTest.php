<?php

declare(strict_types=1);

namespace App\Tests\Domain\Service\Attendance;

use App\Domain\Model\Attendance\ValueObject\AttendanceDate;
use App\Domain\Model\Attendance\ValueObject\AttendanceStatus;
use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Attendance\Attendance;
use App\Domain\Model\Attendance\AttendanceRepository;
use App\Domain\Model\Attendance\ValueObject\AttendanceId;
use App\Domain\Model\Attendance\ValueObject\Attendances;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Domain\Service\Attendance\AttendancesFinder;
use PHPUnit\Framework\TestCase;

class AttendanceFinderTest extends TestCase
{
    public function test_given_data_to_sessions_finder_when_execute_then_return_session_collection()
    {
        $session = Attendance::from(
            AttendanceId::from(1),
            AttendanceDate::from('now'),
            AttendanceStatus::from('2'),
            ClassRoomGroup::from(
                ClassRoomId::from(1),
                GroupId::from(1)
            ),
            StudentId::from(1),
        );
        $sessions = Attendances::from([$session]);

        $repository = $this->createMock(AttendanceRepository::class);
        $repository
            ->expects(self::once())
            ->method('findAllByClassRoomGroup')
            ->with(
                $session->classRoomGroup()->classRoomId(),
                $session->classRoomGroup()->groupId(),
            )
            ->willReturn($sessions);

        $sessionsFinder = new AttendancesFinder($repository);
        $result = $sessionsFinder->execute(
            $session->classRoomGroup()->classRoomId(),
            $session->classRoomGroup()->groupId(),
        );

        self::assertInstanceOf(Attendances::class, $result);
        self::assertEquals($sessions,$result);
    }
}
