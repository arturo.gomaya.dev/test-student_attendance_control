<?php

declare(strict_types=1);

namespace App\Tests\Domain\Service\Student;

use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\Student;
use App\Domain\Model\Student\StudentRepository;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Domain\Model\Student\ValueObject\StudentLastName;
use App\Domain\Model\Student\ValueObject\StudentName;
use App\Domain\Model\Student\ValueObject\Students;
use App\Domain\Service\Student\StudentsFinder;
use PHPUnit\Framework\TestCase;

class StudentsFinderTest extends TestCase
{
    public function test_given_data_to_students_finder_when_execute_then_return_student_collection()
    {
        $student = Student::from(
            StudentId::from(1),
            StudentName::from('name'),
            StudentLastName::from('lastName'),
            ClassRoomGroup::from(
                ClassRoomId::from(1),
                GroupId::from(1)
            ),
        );
        $students = Students::from([$student]);

        $repository = $this->createMock(StudentRepository::class);
        $repository
            ->expects(self::once())
            ->method('findAllByClassRoomGroup')
            ->with(
                $student->classRoomGroup()->classRoomId(),
                $student->classRoomGroup()->groupId(),
            )
            ->willReturn($students);

        $studentsFinder = new StudentsFinder($repository);
        $result = $studentsFinder->execute(
            $student->classRoomGroup()->classRoomId(),
            $student->classRoomGroup()->groupId(),
        );

        self::assertInstanceOf(Students::class, $result);
        self::assertEquals($students,$result);
    }
}
