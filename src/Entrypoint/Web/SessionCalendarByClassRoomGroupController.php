<?php

declare(strict_types=1);

namespace App\Entrypoint\Web;

use App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto\SessionsDto;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\GetSessionsByClassRoomGroupQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

final class SessionCalendarByClassRoomGroupController extends AbstractController
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function __invoke(Request $request)
    {
        $classRoomId = (int) $request->get('classRoomId');

        $sessionsDto = $this->getSessions($classRoomId);

        return $this->render("calendar.html.twig",[
            "classRoomId" => $classRoomId,
            "events" => json_encode($this->sessionsToEvents($sessionsDto)),
        ]);
    }

    private function getSessions(int $classRoomId): SessionsDto
    {
        $query = new GetSessionsByClassRoomGroupQuery($classRoomId);
        return $this->handle($query);
    }

    private function sessionsToEvents(SessionsDto $sessionsDto): array
    {
        $events = [];
        foreach ($sessionsDto as $sessionDto)
        {
            $events[] = [
              "title" => 'G'.$sessionDto->groupId()->value(),
              "start" => $sessionDto->startDate()->getTimestamp() * 1000,
              "end" => $sessionDto->endDate()->getTimestamp() * 1000,
            ];
        }

        return $events;
    }
}