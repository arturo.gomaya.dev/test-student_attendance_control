<?php

declare(strict_types=1);

namespace App\Entrypoint\Web;

use App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto\SessionsDto;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\GetSessionsByClassRoomGroupQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

final class SessionListByClassRoomGroupController extends AbstractController
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function __invoke(Request $request)
    {
        $classRoomId = (int) $request->get('classRoomId');
        $groupId = (int) $request->get('groupId');

        $sessionsDto = $this->getSessions($classRoomId, $groupId);

        return $this->render("sessions.html.twig",[
            "classRoomId" => $classRoomId,
            "groupId" => $groupId,
            "sessions" => $sessionsDto->toArray(),
        ]);
    }

    private function getSessions(int $classRoomId, int $groupId): SessionsDto
    {
        $query = new GetSessionsByClassRoomGroupQuery($classRoomId, $groupId);
        return $this->handle($query);
    }
}