<?php

declare(strict_types=1);

namespace App\Entrypoint\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class HomeController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('base.html.twig',[]);
    }
}