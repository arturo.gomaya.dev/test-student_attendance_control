<?php

declare(strict_types=1);

namespace App\Entrypoint\Web;

use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\Dto\AttendancesDto;
use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\GetAttendancesByClassRoomGroupQuery;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto\SessionsDto;
use App\Application\Query\Session\GetSessionsByClassRoomGroup\GetSessionsByClassRoomGroupQuery;
use App\Application\Query\Student\GetStudentsByClassRoomGroup\Dto\StudentsDto;
use App\Application\Query\Student\GetStudentsByClassRoomGroup\GetStudentsByClassRoomGroupQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

final class AttendanceListByClassRoomGroupController extends AbstractController
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function __invoke(Request $request)
    {
        $classRoomId = (int) $request->get('classRoomId');
        $groupId = (int) $request->get('groupId');

        $attendancesDto = $this->getAttendances($classRoomId, $groupId);
        $studentsDto = $this->getStudents($classRoomId, $groupId);

        $studentsAttendances = $this->getStudentsAttendances($studentsDto,$attendancesDto);

        return $this->render("attendances.html.twig",[
            "classRoomId" => $classRoomId,
            "groupId" => $groupId,
            "students" => $studentsAttendances,
        ]);
    }

    private function getAttendances(int $classRoomId, int $groupId): AttendancesDto
    {
        $query = new GetAttendancesByClassRoomGroupQuery($classRoomId,$groupId);

        return $this->handle($query);
    }

    private function getStudents(int $classRoomId, int $groupId): StudentsDto
    {
        $query = new GetStudentsByClassRoomGroupQuery($classRoomId,$groupId);

        return $this->handle($query);
    }

    private function getStudentsAttendances(StudentsDto $studentsDto, AttendancesDto $attendancesDto): array
    {
        $studentsAttendances = [];
        foreach ($studentsDto as $studentDto)
        {
            $studentsAttendance = $studentDto->jsonSerialize();

            $studentsAttendance['attendances'] = [];
            foreach ($attendancesDto as $attendanceDto)
            {
                if($attendanceDto->studentId()->value() === $studentsAttendance['id'])
                {
                    $studentsAttendance['attendances'][] = $attendanceDto->jsonSerialize();
                }
            }
            $studentsAttendances[] = $studentsAttendance;
        }

        return $studentsAttendances;
    }
}