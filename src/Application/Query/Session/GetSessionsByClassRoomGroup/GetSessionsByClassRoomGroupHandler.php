<?php

declare(strict_types=1);

namespace App\Application\Query\Session\GetSessionsByClassRoomGroup;

use App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto\SessionsDto;
use App\Domain\Service\Session\SessionsFinder;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetSessionsByClassRoomGroupHandler implements MessageHandlerInterface
{
    private SessionsFinder $sessionsFinder;

    public function __construct(SessionsFinder $sessionsFinder)
    {
        $this->sessionsFinder = $sessionsFinder;
    }

    public function __invoke(GetSessionsByClassRoomGroupQuery $query): SessionsDto
    {
        $sessions = $this->sessionsFinder->execute($query->classRoomId(), $query->groupId());

        return SessionsDto::fromSessions($sessions);
    }
}