<?php

declare(strict_types=1);

namespace App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Session\Session;
use App\Domain\Model\Session\ValueObject\SessionEndDate;
use App\Domain\Model\Session\ValueObject\SessionId;
use App\Domain\Model\Session\ValueObject\SessionStartDate;

final class SessionDto implements \JsonSerializable
{
    private SessionId $id;
    private ClassRoomId $classRoomId;
    private GroupId $groupId;
    private SessionStartDate $startDate;
    private SessionEndDate $endDate;

    private function __construct(SessionId $id, ClassRoomId $classRoomId, GroupId $groupId, SessionStartDate $startDate, SessionEndDate $endDate)
    {
        $this->id = $id;
        $this->classRoomId = $classRoomId;
        $this->groupId = $groupId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public static function fromSession(Session $session): self
    {
        return new self(
            $session->id(),
            $session->classRoomGroup()->classRoomId(),
            $session->classRoomGroup()->groupId(),
            $session->startDate(),
            $session->endDate(),
        );
    }

    public function id(): SessionId
    {
        return $this->id;
    }

    public function classRoomId(): ClassRoomId
    {
        return $this->classRoomId;
    }

    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    public function startDate(): SessionStartDate
    {
        return $this->startDate;
    }

    public function endDate(): SessionEndDate
    {
        return $this->endDate;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id->value(),
            "classRoomId" => $this->classRoomId->value(),
            "groupId" => $this->groupId->value(),
            "startDate" => $this->startDate->jsonSerialize(),
            "endDate" => $this->endDate->jsonSerialize(),
        ];
    }
}