<?php

declare(strict_types=1);

namespace App\Application\Query\Session\GetSessionsByClassRoomGroup\Dto;

use App\Domain\Model\Common\CollectionValueObject;
use App\Domain\Model\Session\ValueObject\Sessions;

final class SessionsDto extends CollectionValueObject
{
    public static function fromSessions(Sessions $sessions): self
    {
        $sessionsDto = [];
        foreach ($sessions as $key=>$session){
            $sessionsDto[] = SessionDto::fromSession($session);
        }

        return new self($sessionsDto);
    }

}