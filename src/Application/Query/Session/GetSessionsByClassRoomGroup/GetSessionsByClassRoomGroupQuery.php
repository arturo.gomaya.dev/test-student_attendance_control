<?php

declare(strict_types=1);

namespace App\Application\Query\Session\GetSessionsByClassRoomGroup;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;

final class GetSessionsByClassRoomGroupQuery
{
    private ClassRoomId $classRoomId;
    private ?GroupId $groupId;

    public function __construct(int $classRoomId, ?int $groupId = null)
    {
        $this->classRoomId = ClassRoomId::from($classRoomId);
        $this->groupId = $groupId? GroupId::from($groupId):null;
    }

    public function classRoomId(): ClassRoomId
    {
        return $this->classRoomId;
    }

    public function groupId(): ?GroupId
    {
        return $this->groupId;
    }
}