<?php

declare(strict_types=1);

namespace App\Application\Query\Attendance\GetAttendancesByClassRoomGroup;

use App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\Dto\AttendancesDto;
use App\Domain\Service\Attendance\AttendancesFinder;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetAttendancesByClassRoomGroupHandler implements MessageHandlerInterface
{
    private AttendancesFinder $attendanceFinder;

    public function __construct(AttendancesFinder $attendanceFinder)
    {
        $this->attendanceFinder = $attendanceFinder;
    }

    public function __invoke(GetAttendancesByClassRoomGroupQuery $query): AttendancesDto
    {
        $attendances = $this->attendanceFinder->execute($query->classRoomId(), $query->groupId());

        return AttendancesDto::fromAttendances($attendances);
    }
}