<?php

declare(strict_types=1);

namespace App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\Dto;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Attendance\Attendance;
use App\Domain\Model\Attendance\ValueObject\AttendanceStatus;
use App\Domain\Model\Attendance\ValueObject\AttendanceId;
use App\Domain\Model\Attendance\ValueObject\AttendanceDate;
use App\Domain\Model\Student\ValueObject\StudentId;

final class AttendanceDto implements \JsonSerializable
{
    private AttendanceId $id;
    private ClassRoomId $classRoomId;
    private GroupId $groupId;
    private AttendanceDate $date;
    private AttendanceStatus $status;
    private StudentId $studentId;

    private function __construct(
        AttendanceId $id,
        ClassRoomId $classRoomId,
        GroupId $groupId,
        AttendanceDate $date,
        AttendanceStatus $status,
        StudentId $studentId
    )
    {
        $this->id = $id;
        $this->classRoomId = $classRoomId;
        $this->groupId = $groupId;
        $this->date = $date;
        $this->status = $status;
        $this->studentId = $studentId;
    }

    public static function fromAttendance(Attendance $attendance): self
    {
        return new self(
            $attendance->id(),
            $attendance->classRoomGroup()->classRoomId(),
            $attendance->classRoomGroup()->groupId(),
            $attendance->date(),
            $attendance->status(),
            $attendance->studentId(),
        );
    }

    public function id(): AttendanceId
    {
        return $this->id;
    }

    public function classRoomId(): ClassRoomId
    {
        return $this->classRoomId;
    }

    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    public function date(): AttendanceDate
    {
        return $this->date;
    }

    public function status(): AttendanceStatus
    {
        return $this->status;
    }

    public function studentId(): StudentId
    {
        return $this->studentId;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id->value(),
            "classRoomId" => $this->classRoomId->value(),
            "groupId" => $this->groupId->value(),
            "date" => $this->date->jsonSerialize(),
            "status" => $this->status->value(),
            "studentId" => $this->studentId()->value(),
        ];
    }
}