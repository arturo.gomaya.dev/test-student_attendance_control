<?php

declare(strict_types=1);

namespace App\Application\Query\Attendance\GetAttendancesByClassRoomGroup\Dto;

use App\Domain\Model\Common\CollectionValueObject;
use App\Domain\Model\Attendance\ValueObject\Attendances;

final class AttendancesDto extends CollectionValueObject
{
    public static function fromAttendances(Attendances $sessions): self
    {
        $sessionsDto = [];
        foreach ($sessions as $key=>$session){
            $sessionsDto[] = AttendanceDto::fromAttendance($session);
        }

        return new self($sessionsDto);
    }

}