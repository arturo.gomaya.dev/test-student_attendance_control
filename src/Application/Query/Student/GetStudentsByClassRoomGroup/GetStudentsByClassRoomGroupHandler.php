<?php

declare(strict_types=1);

namespace App\Application\Query\Student\GetStudentsByClassRoomGroup;

use App\Application\Query\Student\GetStudentsByClassRoomGroup\Dto\StudentsDto;
use App\Domain\Service\Student\StudentsFinder;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetStudentsByClassRoomGroupHandler implements MessageHandlerInterface
{
    private StudentsFinder $studentsFinder;

    public function __construct(StudentsFinder $studentsFinder)
    {
        $this->studentsFinder = $studentsFinder;
    }

    public function __invoke(GetStudentsByClassRoomGroupQuery $query): StudentsDto
    {
        $students = $this->studentsFinder->execute($query->classRoomId(), $query->groupId());

        return StudentsDto::fromStudents($students);
    }
}