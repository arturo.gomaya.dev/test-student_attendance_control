<?php

declare(strict_types=1);

namespace App\Application\Query\Student\GetStudentsByClassRoomGroup;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;

final class GetStudentsByClassRoomGroupQuery
{
    private ClassRoomId $classRoomId;
    private GroupId $groupId;

    public function __construct(int $classRoomId, int $groupId)
    {
        $this->classRoomId = ClassRoomId::from($classRoomId);
        $this->groupId = GroupId::from($groupId);
    }

    public function classRoomId(): ClassRoomId
    {
        return $this->classRoomId;
    }

    public function groupId(): GroupId
    {
        return $this->groupId;
    }
}