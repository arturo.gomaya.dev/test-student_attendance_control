<?php

declare(strict_types=1);

namespace App\Application\Query\Student\GetStudentsByClassRoomGroup\Dto;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\ValueObject\StudentLastName;
use App\Domain\Model\Student\ValueObject\StudentName;
use App\Domain\Model\Student\Student;
use App\Domain\Model\Student\ValueObject\StudentId;


final class StudentDto implements \JsonSerializable
{
    private StudentId $id;
    private ClassRoomId $classRoomId;
    private GroupId $groupId;
    private StudentName $name;
    private StudentLastName $lastName;
    

    private function __construct(StudentId $id, ClassRoomId $classRoomId, GroupId $groupId, StudentName $name, StudentLastName $lastName)
    {
        $this->id = $id;
        $this->classRoomId = $classRoomId;
        $this->groupId = $groupId;
        $this->name = $name;
        $this->lastName = $lastName;
    }

    public static function fromStudent(Student $student): self
    {
        return new self(
            $student->id(),
            $student->classRoomGroup()->classRoomId(),
            $student->classRoomGroup()->groupId(),
            $student->name(),
            $student->lastName(),
        );
    }

    public function id(): StudentId
    {
        return $this->id;
    }

    public function classRoomId(): ClassRoomId
    {
        return $this->classRoomId;
    }

    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    public function name(): StudentName
    {
        return $this->name;
    }

    public function lastName(): StudentLastName
    {
        return $this->lastName;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id->value(),
            "classRoomId" => $this->classRoomId->value(),
            "groupId" => $this->groupId->value(),
            "name" => $this->name->jsonSerialize(),
            "lastName" => $this->lastName->jsonSerialize(),
        ];
    }
}