<?php

declare(strict_types=1);

namespace App\Application\Query\Student\GetStudentsByClassRoomGroup\Dto;

use App\Domain\Model\Common\CollectionValueObject;
use App\Domain\Model\Student\ValueObject\Students;

final class StudentsDto extends CollectionValueObject
{
    public static function fromStudents(Students $students): self
    {
        $studentsDto = [];
        foreach ($students as $key=>$student){
            $studentsDto[] = StudentDto::fromStudent($student);
        }

        return new self($studentsDto);
    }

}