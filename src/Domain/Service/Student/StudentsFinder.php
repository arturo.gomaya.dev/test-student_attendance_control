<?php

declare(strict_types=1);

namespace App\Domain\Service\Student;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\StudentRepository;
use App\Domain\Model\Student\ValueObject\Students;

final class StudentsFinder
{
    private StudentRepository $repository;

    public function __construct(StudentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(ClassRoomId $classRoomId, GroupId $groupId): Students
    {
        return $this->repository->findAllByClassRoomGroup($classRoomId, $groupId);
    }
}