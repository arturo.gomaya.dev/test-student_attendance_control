<?php

declare(strict_types=1);

namespace App\Domain\Service\Attendance;

use App\Domain\Model\Attendance\AttendanceRepository;
use App\Domain\Model\Attendance\ValueObject\Attendances;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;

final class AttendancesFinder
{
    private AttendanceRepository $repository;

    public function __construct(AttendanceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(ClassRoomId $classRoomId, ?GroupId $groupId): Attendances
    {
        return $this->repository->findAllByClassRoomGroup($classRoomId, $groupId);
    }
}