<?php

declare(strict_types=1);

namespace App\Domain\Service\Session;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Session\SessionRepository;
use App\Domain\Model\Session\ValueObject\Sessions;

final class SessionsFinder
{
    private SessionRepository $repository;

    public function __construct(SessionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(ClassRoomId $classRoomId, ?GroupId $groupId): Sessions
    {
        return $this->repository->findAllByClassRoomGroup($classRoomId, $groupId);
    }
}