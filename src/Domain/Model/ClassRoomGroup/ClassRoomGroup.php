<?php

declare(strict_types=1);

namespace App\Domain\Model\ClassRoomGroup;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;

final class ClassRoomGroup
{
    private ClassRoomId $classRoomId;
    private GroupId $groupId;

    private function __construct(ClassRoomId $classRoomId, GroupId $groupId)
    {
        $this->classRoomId = $classRoomId;
        $this->groupId = $groupId;
    }

    public static function from(ClassRoomId $classRoomId, GroupId $groupId): self
    {
        return new self($classRoomId, $groupId);
    }

    public function classRoomId(): ClassRoomId
    {
        return $this->classRoomId;
    }

    public function groupId(): GroupId
    {
        return $this->groupId;
    }

}