<?php

declare(strict_types=1);

namespace App\Domain\Model\ClassRoomGroup\ValueObject;

use App\Domain\Model\Common\IntValueObject;

final class ClassRoomId extends IntValueObject
{

}