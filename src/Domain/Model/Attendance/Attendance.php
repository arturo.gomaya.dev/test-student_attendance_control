<?php

declare(strict_types=1);

namespace App\Domain\Model\Attendance;

use App\Domain\Model\Attendance\ValueObject\AttendanceDate;
use App\Domain\Model\Attendance\ValueObject\AttendanceId;
use App\Domain\Model\Attendance\ValueObject\AttendanceStatus;
use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\Student\ValueObject\StudentId;

final class Attendance
{
    private AttendanceId $id;
    private AttendanceDate $date;
    private AttendanceStatus $status;
    private ClassRoomGroup $classRoomGroup;
    private StudentId $studentId;

    private function __construct(
        AttendanceId $id,
        AttendanceDate $date,
        AttendanceStatus $status,
        ClassRoomGroup $classRoomGroup,
        StudentId $studentId
    )
    {
        $this->id = $id;
        $this->date = $date;
        $this->status = $status;
        $this->classRoomGroup = $classRoomGroup;
        $this->studentId = $studentId;
    }

    public static function from(
        AttendanceId $id,
        AttendanceDate $date,
        AttendanceStatus $status,
        ClassRoomGroup $classRoomGroup,
        StudentId $studentId
    ): self
    {
        return new self($id, $date, $status, $classRoomGroup, $studentId);
    }

    public function id(): AttendanceId
    {
        return $this->id;
    }

    public function date(): AttendanceDate
    {
        return $this->date;
    }

    public function status(): AttendanceStatus
    {
        return $this->status;
    }

    public function classRoomGroup(): ClassRoomGroup
    {
        return $this->classRoomGroup;
    }

    public function studentId(): StudentId
    {
        return $this->studentId;
    }

}