<?php

declare(strict_types=1);

namespace App\Domain\Model\Attendance\ValueObject;

use App\Domain\Model\Common\IntValueObject;

final class AttendanceId extends IntValueObject
{

}