<?php

declare(strict_types=1);

namespace App\Domain\Model\Attendance\ValueObject;

use App\Domain\Model\Common\EnumValueObject;

final class AttendanceStatus extends EnumValueObject
{
    private const RETRASO = "1";
    private const PRESENTE = "2";
    private const AUSENTE = "3";
    private const JUSTIFICADO = "4";
}