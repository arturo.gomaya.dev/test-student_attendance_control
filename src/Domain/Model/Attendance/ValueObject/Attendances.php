<?php

declare(strict_types=1);

namespace App\Domain\Model\Attendance\ValueObject;

use App\Domain\Model\Attendance\Attendance;
use App\Domain\Model\Common\CollectionValueObject;
use Webmozart\Assert\Assert;

final class Attendances extends CollectionValueObject
{
    public static function from(array $items): self
    {
        Assert::allIsInstanceOf($items, Attendance::class);

        return new static($items);
    }

    public function add(Attendance $attendance): self
    {
        return $this->addItem($attendance);
    }

    public function remove(Attendance $attendance): self
    {
        return $this->removeItem($attendance);
    }
}