<?php

declare(strict_types=1);

namespace App\Domain\Model\Attendance\ValueObject;

use App\Domain\Model\Common\DateTimeValueObject;

final class AttendanceDate extends DateTimeValueObject
{

}