<?php

declare(strict_types=1);

namespace App\Domain\Model\Attendance;

use App\Domain\Model\Attendance\ValueObject\Attendances;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;

interface AttendanceRepository
{
    public function findAllByClassRoomGroup(ClassRoomId $classRoomId, GroupId $groupId): Attendances;
}