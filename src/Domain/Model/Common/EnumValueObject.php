<?php

declare(strict_types=1);

namespace App\Domain\Model\Common;

use InvalidArgumentException;
use ReflectionClass;

abstract class EnumValueObject implements ValueObject
{
    private string $value;

    private static array $allowedValues;

    final protected function __construct($value)
    {
        $this->guard($value);
        $this->value = $value;
    }

    final public static function allowedValues(): array
    {
        if (!isset(self::$allowedValues[static::class])) {
            $reflection = new ReflectionClass(static::class);
            self::$allowedValues[static::class] = $reflection->getConstants();
        }

        return self::$allowedValues[static::class];
    }

    final public function jsonSerialize(): string
    {
        return $this->value;
    }

    public static function from(string $value)
    {
        return new static($value);
    }

    final public function value(): string
    {
        return $this->value;
    }

    final public function equalTo(self $other): bool
    {
        return static::class === get_class($other)
            && $this->value === $other->value;
    }

    final private function guard($value): void
    {
        if (false === $this->isValid($value)) {
            throw new InvalidArgumentException(
                sprintf(
                    '<%s> not allowed value, allowed values: '
                    . '<%s> for enum class <%s>',
                    $value,
                    implode(' ', static::allowedValues()),
                    static::class,
                ),
            );
        }
    }

    final private function isValid($value): bool
    {
        return in_array($value, static::allowedValues(), true);
    }
}
