<?php

declare(strict_types=1);

namespace App\Domain\Model\Common;

abstract class StringValueObject implements ValueObject
{
    private string $value;

    final protected function __construct(string $value)
    {
        $this->value = $value;
    }

    final public function __toString(): string
    {
        return $this->value;
    }

    final public function value(): string
    {
        return $this->value;
    }

    final public function equalTo(StringValueObject $other): bool
    {
        return static::class === \get_class($other)
            && $this->value === $other->value;
    }

    final public function jsonSerialize(): string
    {
        return $this->value;
    }

    public static function from(string $value): self
    {
        return new static($value);
    }
}
