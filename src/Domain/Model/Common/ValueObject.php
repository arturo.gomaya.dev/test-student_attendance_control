<?php

declare(strict_types=1);

namespace App\Domain\Model\Common;

use JsonSerializable;

interface ValueObject extends JsonSerializable
{
}
