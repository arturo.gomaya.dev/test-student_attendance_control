<?php

declare(strict_types=1);

namespace App\Domain\Model\Session;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Session\ValueObject\Sessions;

interface SessionRepository
{
    public function findAllByClassRoomGroup(ClassRoomId $classRoomId, ?GroupId $groupId): Sessions;
}