<?php

declare(strict_types=1);

namespace App\Domain\Model\Session\ValueObject;

use App\Domain\Model\Common\CollectionValueObject;
use App\Domain\Model\Session\Session;
use Webmozart\Assert\Assert;

final class Sessions extends CollectionValueObject
{
    public static function from(array $items): self
    {
        Assert::allIsInstanceOf($items, Session::class);

        return new static($items);
    }

    public function add(Session $session): self
    {
        return $this->addItem($session);
    }

    public function remove(Session $session): self
    {
        return $this->removeItem($session);
    }

}