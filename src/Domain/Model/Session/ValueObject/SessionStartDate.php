<?php

declare(strict_types=1);

namespace App\Domain\Model\Session\ValueObject;

use App\Domain\Model\Common\DateTimeValueObject;

final class SessionStartDate extends DateTimeValueObject
{

}