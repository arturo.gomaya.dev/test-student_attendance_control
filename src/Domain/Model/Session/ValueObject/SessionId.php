<?php

declare(strict_types=1);

namespace App\Domain\Model\Session\ValueObject;

use App\Domain\Model\Common\IntValueObject;

final class SessionId extends IntValueObject
{

}