<?php

declare(strict_types=1);

namespace App\Domain\Model\Session;

use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\Session\ValueObject\SessionEndDate;
use App\Domain\Model\Session\ValueObject\SessionId;
use App\Domain\Model\Session\ValueObject\SessionStartDate;

final class Session
{
    private SessionId $id;
    private ClassRoomGroup $classRoomGroup;
    private SessionStartDate $startDate;
    private SessionEndDate $endDate;

    private function __construct(SessionId $id, ClassRoomGroup $classRoomGroup, SessionStartDate $startDate, SessionEndDate $endDate)
    {
        $this->id = $id;
        $this->classRoomGroup = $classRoomGroup;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public static function from(SessionId $id, ClassRoomGroup $classRoomGroup, SessionStartDate $startDate, SessionEndDate $endDate): self
    {
        return new self($id, $classRoomGroup, $startDate, $endDate);
    }

    public function id(): SessionId
    {
        return $this->id;
    }

    public function classRoomGroup(): ClassRoomGroup
    {
        return $this->classRoomGroup;
    }

    public function startDate(): SessionStartDate
    {
        return $this->startDate;
    }

    public function endDate(): SessionEndDate
    {
        return $this->endDate;
    }

}