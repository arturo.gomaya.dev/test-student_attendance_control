<?php

declare(strict_types=1);

namespace App\Domain\Model\Student;

use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Domain\Model\Student\ValueObject\StudentLastName;
use App\Domain\Model\Student\ValueObject\StudentName;

final class Student
{
    private StudentId $id;
    private StudentName $name;
    private StudentLastName $lastName;
    private ClassRoomGroup $classRoomGroup;

    private function __construct(StudentId $id, StudentName $name, StudentLastName $lastName, ClassRoomGroup $classRoomGroup)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lastName = $lastName;
        $this->classRoomGroup = $classRoomGroup;
    }

    public static function from(StudentId $id, StudentName $name, StudentLastName $lastName, ClassRoomGroup $classRoomGroup): self
    {
        return new self($id, $name, $lastName, $classRoomGroup);
    }

    public function id(): StudentId
    {
        return $this->id;
    }

    public function name(): StudentName
    {
        return $this->name;
    }

    public function lastName(): StudentLastName
    {
        return $this->lastName;
    }

    public function classRoomGroup(): ClassRoomGroup
    {
        return $this->classRoomGroup;
    }
}