<?php

declare(strict_types=1);

namespace App\Domain\Model\Student\ValueObject;

use App\Domain\Model\Common\CollectionValueObject;
use App\Domain\Model\Student\Student;
use Webmozart\Assert\Assert;

final class Students extends CollectionValueObject
{
    public static function from(array $items): self
    {
        Assert::allIsInstanceOf($items, Student::class);

        return new static($items);
    }

    public function add(Student $session): self
    {
        return $this->addItem($session);
    }

    public function remove(Student $session): self
    {
        return $this->removeItem($session);
    }

}