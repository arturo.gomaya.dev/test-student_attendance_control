<?php

declare(strict_types=1);

namespace App\Domain\Model\Student\ValueObject;

use App\Domain\Model\Common\IntValueObject;

final class StudentId extends IntValueObject
{

}