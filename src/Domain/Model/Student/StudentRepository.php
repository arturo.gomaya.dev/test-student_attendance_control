<?php

declare(strict_types=1);

namespace App\Domain\Model\Student;

use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\ValueObject\Students;

interface StudentRepository
{
    public function findAllByClassRoomGroup(ClassRoomId $classRoomId, GroupId $groupId): Students;
}