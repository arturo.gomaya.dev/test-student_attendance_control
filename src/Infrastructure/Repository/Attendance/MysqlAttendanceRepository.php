<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Attendance;

use App\Domain\Model\Attendance\ValueObject\AttendanceDate;
use App\Domain\Model\Attendance\ValueObject\AttendanceStatus;
use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Attendance\Attendance;
use App\Domain\Model\Attendance\AttendanceRepository;
use App\Domain\Model\Attendance\ValueObject\AttendanceId;
use App\Domain\Model\Attendance\ValueObject\Attendances;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Infrastructure\Repository\MySqlRepository;
use PDO;

final class MysqlAttendanceRepository extends MySqlRepository implements AttendanceRepository
{

    public function findAllByClassRoomGroup(ClassRoomId $classRoomId, GroupId $groupId): Attendances
    {
        $query = $this
            ->connection
            ->prepare(
                "SELECT * FROM cl_aulas_attendance WHERE aula_id=:aulaId AND group_id=:groupId"
            );
        $query->execute([
            "aulaId"=>$classRoomId->value(),
            "groupId"=>$groupId->value(),
        ]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $attendances = [];
        foreach ($result as $row)
        {
            $attendances[] = $this->mapToAttendance($row);
        }

        return Attendances::from($attendances);
    }

    private function mapToAttendance(array $data): Attendance
    {
        return Attendance::from(
            AttendanceId::from((int) $data['attendance_id']),
            AttendanceDate::from($data['attendance_date']),
            AttendanceStatus::from($data['attendance_status']),
            ClassRoomGroup::from(
                ClassRoomId::from((int) $data['aula_id']),
                GroupId::from((int) $data['group_id']),
            ),
            StudentId::from((int) $data['student_id']),
        );
    }
}