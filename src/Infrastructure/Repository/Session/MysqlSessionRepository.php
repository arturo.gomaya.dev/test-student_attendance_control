<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Session;

use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Session\Session;
use App\Domain\Model\Session\SessionRepository;
use App\Domain\Model\Session\ValueObject\SessionEndDate;
use App\Domain\Model\Session\ValueObject\SessionId;
use App\Domain\Model\Session\ValueObject\Sessions;
use App\Domain\Model\Session\ValueObject\SessionStartDate;
use App\Infrastructure\Repository\MySqlRepository;
use PDO;

final class MysqlSessionRepository extends MySqlRepository implements SessionRepository
{

    public function findAllByClassRoomGroup(ClassRoomId $classRoomId, ?GroupId $groupId): Sessions
    {
        $args = ["aulaId"=>$classRoomId->value()];
        if(null !== $groupId) $args['groupId'] = $groupId->value();
        $query = $this
            ->connection
            ->prepare(
                "SELECT * FROM cl_aulas_calendar WHERE aula_id=:aulaId" . (null !== $groupId? " AND group_id=:groupId" : "")
            );
        $query->execute($args);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $sessions = [];
        foreach ($result as $row)
        {
            $sessions[] = $this->mapToSession($row);
        }

        return Sessions::from($sessions);
    }

    private function mapToSession(array $data): Session
    {
        return Session::from(
            SessionId::from((int) $data['aula_calendar_id']),
            ClassRoomGroup::from(
                ClassRoomId::from((int) $data['aula_id']),
                GroupId::from((int) $data['group_id']),
            ),
            SessionStartDate::from($data['calendar_date_ini']),
            SessionEndDate::from($data['calendar_date_end']),
        );
    }
}