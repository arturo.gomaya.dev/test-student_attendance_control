<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Student;

use App\Domain\Model\ClassRoomGroup\ClassRoomGroup;
use App\Domain\Model\ClassRoomGroup\ValueObject\ClassRoomId;
use App\Domain\Model\ClassRoomGroup\ValueObject\GroupId;
use App\Domain\Model\Student\Student;
use App\Domain\Model\Student\StudentRepository;
use App\Domain\Model\Student\ValueObject\StudentId;
use App\Domain\Model\Student\ValueObject\StudentLastName;
use App\Domain\Model\Student\ValueObject\StudentName;
use App\Domain\Model\Student\ValueObject\Students;
use App\Infrastructure\Repository\MySqlRepository;
use PDO;

final class MysqlStudentRepository extends MySqlRepository implements StudentRepository
{

    public function findAllByClassRoomGroup(ClassRoomId $classRoomId, GroupId $groupId): Students
    {
        $query = $this
            ->connection
            ->prepare(
                "SELECT * FROM cl_students as stu 
                LEFT JOIN cl_aulas_students as aul ON stu.student_id = aul.student_id 
                WHERE aula_id=:aulaId AND group_id=:groupId"
            );
        $query->execute([
            "aulaId"=>$classRoomId->value(),
            "groupId"=>$groupId->value(),
        ]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $students = [];
        foreach ($result as $row)
        {
            $students[] = $this->mapToStudent($row);
        }

        return Students::from($students);
    }

    private function mapToStudent(array $data): Student
    {
        return Student::from(
            StudentId::from((int) $data['student_id']),
            StudentName::from($data['student_name'] ?? ''),
            StudentLastName::from($data['student_lastname'] ?? ''),
            ClassRoomGroup::from(
                ClassRoomId::from((int) $data['aula_id']),
                GroupId::from((int) $data['group_id']),
            )
        );
    }
}