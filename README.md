Instrucciones

1. Conectar la base de datos MySQL con PDO a la BBDD adjunta (PruebaTecnicaAsistencias.sql).
2. Crear un controlador de Asistencia que permita generar informes de asistencia y despachar vistas. 
3. Diseñar las 3 vistas de informes para presentar información.

Importante: 
- No es necesario utilizar un Framework.
- Se adjuntan referencias gráficas a partir de los estilos de la aplicación que ya existen actualmente, se puede presentar sin estilos las vistas. 
- No se valorará diseño CSS para los estilos de las vistas. 
- En una misma aula pueden convivir diferentes grupos (identificados como grupo 1 y grupo 2) quienes tienen diferentes horarios y asistencias.


Vistas a generar:
1. Listado de las sesiones de calendario por cada aula / grupo.

2. Listado de los alumnos con el porcentaje de Asistencia por alumno en base al numero total de horas y las ausencias que han tenido diferenciando SESIONES de HORAS según el aula seleccionada en GET indicando los parámetros [ aula_id=#&group_id=# ].    *Los justificados no restan porcentaje.


3. Utilizando la librería FULLCALENDAR.IO (https://fullcalendar.io/) desplegar las sesiones de calendario de las aulas 
3.1. Se iniciará la vista en el mes de MARZO 2018 utilizando como nombre AULA ###. 


3.2. Se deberá definir el control de inicio y fin de cada sesión para desplegar la vista semanal co los limites. 





Las tablas de la BBDD para el proyecto incluyen:
cl_attendance_status [ Estado de la asistencia ]
cl_aulas_attendance [ Control de asistencia por aula por grupo por alumno ]
cl_aulas_calendar [ Control de las sesiones programadas en el calendario ]
cl_aulas_students [ Control de los alumnos registrados por aula por grupo ]
cl_students [ Datos de los alumnos ]

El proyecto se deberá entregar respondiendo a este mensaje con: 
1. Enlace LOOM de la grabación de video.
2. Fichero ZIP con las carpetas utilizadas para el desarrollo del proyecto. 
